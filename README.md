Log saver
=====

Reading from rabbit queue and save it to db. Pretty simple

Setup
=====

`sudo apt-get install libpq-dev python3-dev`

Configuration service
=====

To run notifier on system startup (systemd) edit WorkingDirectory and ExecStart params
in logsaver.service and then copy file to /lib/systemd/system/ directory

chmod 644 logsaver.service
sudo systemctl daemon-reload
sudo systemctl enable notifier.service

App will start on next reboot.
To run immediately type sudo systemctl start logsaver.service