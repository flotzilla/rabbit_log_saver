from tendo import singleton
from tendo.singleton import SingleInstanceException
from log_saver.entity.Logs import Log
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker
from _datetime import datetime

import pika
import pika.exceptions
import config
import json

from log_saver.utils.utils import Utils


class App:
    def __init__(self):
        credentials = pika.PlainCredentials(config.rabbit_user, config.rabbit_password)
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=config.rabbit_host,
            port=config.rabbit_port,
            virtual_host=config.rabbit_vhost,
            credentials=credentials))
        self.channel = self.connection.channel()
        self.engine = create_engine(config.db_url)

    def configure_consumer(self):
        def on_receive_messages(channel, method, properties, body):
            resp = self.save_to_bd(body)
            if resp is not False:
                channel.basic_ack(delivery_tag=method.delivery_tag)
            else:
                channel.cancel()

        self.channel.queue_declare(queue=config.rabbit_logging_queue, durable=True)
        self.channel.queue_bind(queue=config.rabbit_logging_queue,
                                exchange='alarms')
        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(queue=config.rabbit_logging_queue,
                                   consumer_callback=on_receive_messages)

    def save_to_bd(self, body):
        connection = self.engine.connect()
        body = json.loads(body.decode('utf-8').replace("'", '"'))
        status = False
        try:
            Session = sessionmaker(bind=self.engine)
            session = Session()

            if 'message' in body:
                message = body['message']
            else:
                message = ""

            if 'message_stack' in body:
                message_stack = body['message_stack']
            else:
                message_stack = "{}"

            logger.debug('Saving request')

            log = Log(type=body['type'],
                      level=body['level'],
                      message=message,
                      timestamp=datetime.fromtimestamp(body['timestamp']),
                      message_stack=message_stack)

            session.add(log)
            session.commit()
            status = True
        except Exception as e:
            logger.error(e)
        finally:
            connection.close()

        return status


if __name__ == '__main__':
    logger = Utils.init_logger()
    logger.info('starting log saver')
    try:
        me = singleton.SingleInstance()
        app = App()
        app.configure_consumer()

        try:
            app.channel.start_consuming()
        except Exception as e:
            app.channel.stop_consuming()
            raise e
        finally:
            if not app.connection.is_closed:
                app.connection.close()

    except SingleInstanceException as single:
        logger.info('Already running')
    except KeyboardInterrupt as kbi:
        logger.info('Stopping process')
    except Exception as e:
        logger.error(e)
