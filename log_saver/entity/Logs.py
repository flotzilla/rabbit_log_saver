from sqlalchemy import Column, Integer, String, Text, JSON, TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Log(Base):
    __tablename__ = 'logs'

    id = Column(Integer, primary_key=True)
    type = Column(String)
    level = Column(String)
    message = Column(Text)
    message_stack = Column(JSON)
    timestamp = Column(TIMESTAMP)

    def __init__(self, type, level, message, message_stack, timestamp):
        self.type = type
        self.level = level
        self.message = message
        self.message_stack = message_stack
        self.timestamp = timestamp
