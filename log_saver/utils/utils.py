import logging
import config


class Utils:
    @staticmethod
    def init_logger():
        logger = logging.getLogger('db_saver')
        logger.setLevel(level=logging.getLevelName(config.log_level))

        fh = logging.FileHandler('app.log')
        fh.setLevel(logging.getLevelName(config.file_log_level))
        ch = logging.StreamHandler()
        ch.setLevel(logging.getLevelName(config.log_level))

        formatter = logging.Formatter('[%(asctime)s][%(name)s][%(levelname)s][%(filename)s:%(lineno)d] - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        logger.addHandler(fh)
        logger.addHandler(ch)

        return logger
