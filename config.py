db_type = 'postgresql'
db_user = 'postgres'
db_pass = 'postgres'
db_name = 'postgres'
db_host = '192.168.1.20'
db_port = '5432'

db_url = db_type + '://' + db_user + ':' + db_pass + '@' + db_host + ':' + db_port + '/' + db_name

rabbit_host = '192.168.1.20'
rabbit_port = 5672
rabbit_user = 'test'
rabbit_password = 'test'
rabbit_vhost = 'home_servar'
rabbit_logging_queue = 'logging'

log_level = 'DEBUG'
file_log_level = 'DEBUG'


